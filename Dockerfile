FROM node:16.15.0-alpine

RUN apk add --no-cache git

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY . .

ARG YANDEX_METRIKA_ID
ENV YANDEX_METRIKA_ID $YANDEX_METRIKA_ID

RUN npm run build