# Авторизация

## Описание процесса авторизации

![Демонстрация авторизации](./images/main/auth.gif)

![Схема процесса авторизации](./images/main/auth.png)

Авторизация в системе происходит с помощью протокола [OAuth](https://www.digitalocean.com/community/tutorials/oauth-2-ru). 

::: tip
В рамках данной статьи будет описана авторизация с помощью **кода авторизации (authorization code flow)**. Этот способ подходит для серверных приложений.
:::

![](./images/oauth/Authorization-Code-Flow-Russian@2x.png)

_Диаграмма из [статьи про OAuth от DigitalOcean](https://www.digitalocean.com/community/tutorials/oauth-2-ru)_

1. Пользователь переходит на веб-сайт, нажимает на кнопку "Войти". После нажатия на нее он переходит на страницу с формой для ввода логина и пароля.
2. После ввода логина и пароля приложение авторизует пользователя, создается код авторизации.
3. Авторизационный сервер переадресовывает пользователя обратно в приложение с кодом авторизации.
4. Приложение запрашивает токен доступа с помощью кода авторизации.
5. Токен доступа получен, можно производить запросы.

## Регистрация приложения
Для начала работы нужно зарегистрировать приложение в системе и получить `client_id` и `client_secret`. Во время регистрации приложения нужно будет указать список адресов, на которые будет перебрасываться пользователь после завершения процесса авторизации.

## Отправка пользователя на сервер авторизации

Для начала веб-сайт должен перенаправить пользователя на сервер авторизации по следующей ссылке:
<pre><code>{{ CORE_URL }}/oauth/authorize?response_type=code&client_id=CLIENT_ID&redirect_uri=CALLBACK_URL</code></pre>

- `CLIENT_ID` - `client_id` приложения
- `CALLBACK_URL` - адрес, на который пользователь будет возвращен после авторизации

У пользователя открывается форма входа, в которой он вводит логин и пароль. В случае успешной авторизации, пользователь переадресовывается на адрес, который был указан вместо `CALLBACK_URL`. К этому адресу добавляется параметр с кодом авторизации: `?code=AUTHORIZATION_CODE`

## Запрос токена по коду авторизации

Для получения токена нужно взять GET-параметр `code` и сделать следующий запрос:

- Метод: `POST`
- URL: <code>{{ CORE_URL }}/oauth/token/</code>
- Заголовок: `Content-Type: application/x-www-form-urlencoded`

**Тело запроса:**

```
client_id=CLIENT_ID
client_secret=CLIENT_SECRET
grant_type=authorization_code
code=CODE
redirect_uri=CALLBACK_URL
```

- `CLIENT_ID` - `client_id` приложения
- `CLIENT_SECRET` - `client_secret` приложения
- `CODE` - код авторизации из GET-параметра
- `CALLBACK_URL` - URL, на который переадресовывался пользователь ранее

В **ответ** придет токен, с помощью которого можно будет выполнять запросы к API:

```json{2}
{
    "access_token": "ACCESS_TOKEN",
    "refresh_token": "REFRESH_TOKEN",
    "expires_in": 36000,
    "token_type": "Bearer",
    "scope": "read write"
}
```

В параметре `expires_in` содержится время в секундах, в течение которого токен будет активен. Для того, чтобы получить новый токен, нужно его обновить - об этом написано ниже.

Пример запроса на CURL:

<div class="language-bash"><pre class="language-json"><code>curl -X POST {{ CORE_URL }}/oauth/token/ \
-H 'content-type: application/x-www-form-urlencoded' \
-d 'client_id=CLIENT_ID&client_secret=CLIENT_SECRET&grant_type=authorization_code&redirect_uri=CALLBACK_URL'
</code></pre></div>

## Получение профиля

Нужно выполнить GET-запрос с авторизационным заголовком, в котором будет находиться токен, полученный на предыдущем шаге:

- Метод: `GET`
- URL: <code>{{ CORE_URL }}/api/v1.0/profile/</code> 
- Заголовок: `Authorization: Bearer ACCESS_TOKEN`

В ответ придет JSON с профилем пользователя. Подробнее о его структуре можно прочесть в <a :href="`${CORE_URL}/docs/redoc/#operation/profile_list`" target="_blank">документации</a>.

## Обновление токена

Для получения нового access token с помощью refresh token нужно выполнить запрос:

- Метод: `POST`
- URL: <code>{{ CORE_URL }}/oauth/token/</code>
- Заголовок: `Content-Type: application/x-www-form-urlencoded`

**Тело запроса:**

```
grant_type=refresh_token
refresh_token=REFRESH_TOKEN
client_id=CLIENT_ID
client_secret=CLIENT_SECRET
```

- `CLIENT_ID` - `client_id` приложения
- `CLIENT_SECRET` - `client_secret` приложения
- `REFRESH_TOKEN` - `refresh_token` полученный при запросе токена ранее

В ответ придет та же самая информация, как и при первом получении токена:

```json{2}
{
    "access_token": "ACCESS_TOKEN",
    "refresh_token": "REFRESH_TOKEN",
    "expires_in": 36000,
    "token_type": "Bearer",
    "scope": "read write"
}
```

## Отзыв токена (выход)

Когда пользователь выходит из приложения, нужно отозвать действующий токен. Это можно с помощью запроса:

- Метод: `POST`
- URL: <code>{{ CORE_URL }}/oauth/revoke_token/</code>
- Заголовок: `Content-Type: application/x-www-form-urlencoded`

**Тело запроса:**

```
token=ACCESS_TOKEN
client_id=CLIENT_ID
client_secret=CLIENT_SECRET
```

- `CLIENT_ID` - `client_id` приложения
- `CLIENT_SECRET` - `client_secret` приложения
- `ACCESS_TOKEN` - токен, полученный ранее

Если ответ пришел с 200 кодом, значит токен успешно отозван.
