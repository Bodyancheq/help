# Технические детали

- **Серверная часть**: Python 3, Django 2, PostgreSQL, nginx
- **Клиентская часть**: VueJS, Nuxt.js, xterm.js
- **Развертывание инфраструктурных сервисов**: Ansible, Terraform, Docker, Gitlab CI
- **Развертывание студенческих приложений**: Gitlab CI, Docker, Dokku, Heroku, Python, Flask
